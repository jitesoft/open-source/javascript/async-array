# async-array

[![npm (scoped)](https://img.shields.io/npm/v/@jitesoft/async-array)](https://www.npmjs.com/package/@jitesoft/async-array)
[![Known Vulnerabilities](https://dev.snyk.io/test/npm/@jitesoft/async-array/badge.svg)](https://dev.snyk.io/test/npm/@jitesoft/async-array)
[![pipeline status](https://gitlab.com/jitesoft/open-source/javascript/async-array/badges/master/pipeline.svg)](https://gitlab.com/jitesoft/open-source/javascript/async-array/commits/master)
[![coverage report](https://gitlab.com/jitesoft/open-source/javascript/async-array/badges/master/coverage.svg)](https://gitlab.com/jitesoft/open-source/javascript/async-array/commits/master)
[![npm](https://img.shields.io/npm/dt/@jitesoft/async-array)](https://www.npmjs.com/package/@jitesoft/async-array)
[![Back project](https://img.shields.io/badge/Open%20Collective-Tip%20the%20devs!-blue.svg)](https://opencollective.com/jitesoft-open-source)

Array wrapper with async methods.

There are multiple ways to use this package:

## Static functions

Import the static methods which are possible to use on any array type object:


````js
import {map} from '@jitesoft/async-array';
import {doFunStuffAsync} from './async-functions';

(async () => {
let result = await map(['a', 'b', 'c'], async (value, index, list) => {
  let somethingSomething = await doFunStuffAsync(value);
});
})();
````

The following methods are available to import directly for usage:

* `every`
* `find`
* `findIndex`
* `forEach` 
* `forEachSeries`
* `map`
* `some`

## AsyncArray class

The `AsyncArray` class is a class which extends the Array class. All the standard methods that the array class have is 
available on the `AsyncArray`. The following methods are overriden to introduce async functionality:

* `.every`
* `.find`
* `.findIndex`
* `.forEach`
* `.series`
* `.map`
* `.some`

## Convert Array

It's also possible to import the simple `async` and `sync` methods directly from the package to convert a normal array into
async or async array into normal.

```js
import {sync, async} from '@jitesoft/async-array';

let array = [];       // Array
array = async(array); // AsyncArray
array = sync(array);  // Array
```
