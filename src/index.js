import { every, find, findIndex, forEach, forEachSeries, map, some } from '../src/Static';

class AsyncArray extends Array {
  constructor (...items) {
    super();
    if (items.length > 0) {
      this.push(...items);
    }
  }
  /**
   * Performs the specified action for each element in an array.
   *
   * @param {function} callbackfn A function that accepts up to three arguments.
   *                              forEach calls the callbackfn function one time for each element in the array.
   * @param {*}        [thisArg]  An object to which the this keyword can refer in the callbackfn function.
   *                              If thisArg is omitted, undefined is used as the this value.
   * @return {Promise<void>}
   */
  async forEach (callbackfn, thisArg = undefined) {
    return forEach(this, callbackfn, thisArg);
  }

  /**
   * Calls a defined callback function on each element of an array, and returns an array that contains the results.
   *
   * @param {function} callbackfn A function that accepts up to three arguments.
   *                              The map method calls the callbackfn function one time for each element in the array.
   * @param {*}        [thisArg]  An object to which the this keyword can refer in the callbackfn function.
   *                              If thisArg is omitted, undefined is used as the this value.
   * @return {Promise<Array<*>>}
   */
  async map (callbackfn, thisArg = undefined) {
    return map(this, callbackfn, thisArg);
  }

  /**
   * Determines whether the specified callback function returns true for any element of an array.
   *
   * @param {function} callbackfn A function that accepts up to three arguments.
   *                              The some method calls the callbackfn function for each element in array1 until
   *                              the callbackfn returns true, or until the end of the array.
   * @param {*}        [thisArg]  An object to which the this keyword can refer in the callbackfn function.
   *                              If thisArg is omitted, undefined is used as the this value.
   *
   * @return {Promise<boolean>}
   */
  async some (callbackfn, thisArg = undefined) {
    return some(this, callbackfn, thisArg);
  }

  /**
   * Returns the index of the first element in the array where predicate is true, and -1 otherwise.
   *
   * @param {function} predicate find calls predicate once for each element of the array, in ascending
   *                             order, until it finds one where predicate returns true. If such an element is found,
   *                             findIndex immediately returns that element index. Otherwise, findIndex returns -1.
   * @param {*}       [thisArg] If provided, it will be used as the this value for each invocation of
   *                            predicate. If it is not provided, undefined is used instead.
   *
   * @return {Promise<number>}
   */
  async findIndex (predicate, thisArg = undefined) {
    return findIndex(this, predicate, thisArg);
  }

  /**
   * Returns the value of the first element in the array where predicate is true, and undefined otherwise.
   *
   * @param {function} predicate find calls predicate once for each element of the array, in ascending
   *                             order, until it finds one where predicate returns true. If such an element is found, find
   *                             immediately returns that element value. Otherwise, find returns undefined.
   * @param {*}        [thisArg] If provided, it will be used as the this value for each invocation of
   *                             predicate. If it is not provided, undefined is used instead.
   *
   * @return {Promise<*>}
   */
  async find (predicate, thisArg = undefined) {
    return find(this, predicate, thisArg);
  }

  /**
   * Performs the specified action for each element in an array in order.
   *
   * @param {function} callbackfn A function that accepts up to three arguments. forEach calls the callbackfn
   *                              function one time for each element in the array.
   * @param {*}        [thisArg]  An object to which the this keyword can refer in the callbackfn function.
   *                              If thisArg is omitted, undefined is used as the this value.
   *
   * @return {Promise<void>}
   */
  async series (callbackfn, thisArg = undefined) {
    await forEachSeries(this, callbackfn, thisArg);
  }

  /**
   * Determines whether all the members of an array satisfy the specified test.
   *
   * @param {function} callbackfn A function that accepts up to three arguments.
   *                              The every method calls the callbackfn function for each element in the array
   *                              until the callbackfn returns false, or until the end of the array.
   * @param {*}        [thisArg]  An object to which the this keyword can refer in the callbackfn function.
   *                              If thisArg is omitted, undefined is used as the this value.
   *
   * @return {Promise<boolean>} Promise which resolves to true or false depending on outcome.
   */
  async every (callbackfn, thisArg = undefined) {
    return every(this, callbackfn, thisArg);
  }
}

export default AsyncArray;

/**
 * Convert an array to a AsyncArray.
 *
 * @param {array} array Array to convert.
 * @returns {array|AsyncArray}
 */
const async = (array) => (new AsyncArray(...array));

/**
 * Convert an AsyncArray to a normal array.
 *
 * @param {AsyncArray|array} array Array to convert.
 * @returns {array}
 */
const sync = (array) => [...array];

export {
  async,
  sync,
  AsyncArray,
  every,
  find,
  findIndex,
  forEach,
  forEachSeries,
  map,
  some
};
