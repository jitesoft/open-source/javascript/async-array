import { async, AsyncArray } from './index';
const proto = Array.prototype;

/**
 * Performs the specified action for each element in an array.
 *
 * @param {array}    list       Array to loop through.
 * @param {function} callbackfn A function that accepts up to three arguments.
 *                              forEach calls the callbackfn function one time for each element in the array.
 * @param {*}       [thisArg]   An object to which the this keyword can refer in the callbackfn function.
 *                              If thisArg is omitted, undefined is used as the this value.
 */
const forEach = async (list, callbackfn, thisArg = undefined) => {
  const promises = proto.map.call(list, (value, index, arr) => Promise.resolve().then(() => {
    return callbackfn.call(thisArg, value, index, arr);
  }));

  await Promise.all(promises);
};

/**
 * Performs the specified action for each element in an array in order, waiting for the previous to finish before calling next.
 *
 * @param {array}    list       Array to loop through.
 * @param {function} callbackfn A function that accepts up to three arguments.
 *                              forEach calls the callbackfn function one time for each element in the array.
 * @param {*}        [thisArg]  An object to which the this keyword can refer in the callbackfn function.
 *                              If thisArg is omitted, undefined is used as the this value.
 */
const forEachSeries = async (list, callbackfn, thisArg = undefined) => {
  const c = list.length;
  for (let i = 0; i < c; i++) {
    await callbackfn.call(thisArg, list[i], i, list);
  }
};

/**
 * Calls a defined callback function on each element of an array, and returns an array that contains the results.
 *
 * @param {array}    list       Array to loop through.
 * @param {function} callbackfn A function that accepts up to three arguments.
 *                              The map method calls the callbackfn function one time for each element in the array.
 * @param {*}        [thisArg]  An object to which the this keyword can refer in the callbackfn function.
 *                              If thisArg is omitted, undefined is used as the this value.
 *
 * @return {Promise<Array<*>>} Resulting array.
 */
const map = async (list, callbackfn, thisArg = undefined) => {
  const result = [];
  const promises = proto.map.call(list, (value, index, arr) => Promise.resolve().then(() => {
    return callbackfn.call(thisArg, value, index, arr);
  }).then((i) => { result[index] = i; }));

  await Promise.all(promises);
  if (list instanceof AsyncArray) {
    return async(result);
  }
  return result;
};

/**
 * Returns the value of the first element in the array where predicate is true, and undefined
 * otherwise.
 * @param {array}    list       Array to loop through.
 * @param {function} callbackfn Find calls callbackfn once for each element of the array, in ascending
 *                              order, until it finds one where predicate returns true. If such an element is found, find
 *                              immediately returns that element value. Otherwise, find returns undefined.
 * @param {*}       [thisArg]   If provided, it will be used as the this value for each invocation of
 *                              predicate. If it is not provided, undefined is used instead.
 *
 * @return {Promise<*|undefined>} Promise resolving to either the object found or undefined.
 */
const find = async (list, callbackfn, thisArg = undefined) => {
  let result = null;
  const promises = proto.map.call(list, (value, index, arr) => {
    return Promise.resolve()
      .then(() => callbackfn.call(thisArg, value, index, arr))
      .then((res) => {
        if (res) {
          result = value;
          throw new Error();
        }
      });
  });

  try {
    await Promise.all(promises);
    return undefined;
  } catch (e) {
    return result;
  }
};

/**
 * Determines whether the specified callback function returns true for any element of an array.
 *
 * @param {array}    list Array to loop through.
 * @param {function} callbackfn A function that accepts up to three arguments.
 *                              The some method calls the callbackfn function for each element in array until the
 *                              callbackfn returns true, or until the end of the array.
 * @param {*}        [thisArg] An object to which the this keyword can refer in the callbackfn function.
 *                             If thisArg is omitted, undefined is used as the this value.
 *
 * @return {Promise<boolean>} Promise resolving to either true or false depending on the outcome.
 */
const some = async (list, callbackfn, thisArg = undefined) => {
  return await find(list, callbackfn, thisArg) !== undefined;
};

/**
 * Returns the index of the first element in the array where predicate is true, and -1
 * otherwise.
 *
 * @param {array}    list Array to loop through.
 * @param {function} callbackfn find calls callbackfn once for each element of the array, in ascending
 *                              order, until it finds one where predicate returns true. If such an element is found,
 *                              findIndex immediately returns that element index. Otherwise, findIndex returns -1.
 * @param {*}        [thisArg] If provided, it will be used as the this value for each invocation of
 *                              predicate. If it is not provided, undefined is used instead.
 *
 * @return {Promise<Number>} Promise resolving to either -1 or the resulting index depending on outcome.
 */
const findIndex = async (list, callbackfn, thisArg = undefined) => {
  let result = null;
  const promises = proto.map.call(list, (value, index, arr) => {
    return Promise.resolve().then(() => {
      return callbackfn.call(thisArg, value, index, arr);
    }).then((res) => {
      if (res) {
        result = index;
        throw new Error();
      }
    });
  });

  try {
    await Promise.all(promises);
    return -1;
  } catch (e) {
    return result;
  }
};

/**
 * Determines whether all the members of an array satisfy the specified test.
 *
 * @param {array}    list       Array to loop through.
 * @param {function} callbackfn A function that accepts up to three arguments.
 *                              The every method calls the callbackfn function for each element in array until
 *                              the callbackfn returns false, or until the end of the array.
 * @param {*}        [thisArg]  An object to which the this keyword can refer in the callbackfn function.
 *                              If thisArg is omitted, undefined is used as the this value.
 *
 * @return {Promise<boolean>} Promise which resolves to true or false depending on outcome.
 */
const every = async (list, callbackfn, thisArg = undefined) => {
  return await findIndex(list, (item, index, array) => {
    return !callbackfn.call(thisArg, item, index, array);
  }, thisArg) === -1;
};

export {
  forEach,
  forEachSeries,
  map,
  find,
  some,
  every,
  findIndex
};
