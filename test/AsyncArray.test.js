import { AsyncArray } from '../src/index';

const items = new AsyncArray('a', 'b', 3, 7, 'hej', { a: 'b' }, null, 41, 'hi!', 'cat', { spec: 'dog', ears: 1 });

describe('Tests AsyncArray forEach.', () => {
  test('Foreach calls the callback for each item.', async () => {
    const fn = jest.fn();
    await items.forEach(fn);
    expect(fn).toHaveBeenCalledTimes(items.length);

    for (let i = 0; i < items.length; i++) {
      expect(fn).toHaveBeenCalledWith(items[i], expect.any(Number), items);
    }
    expect(fn).toHaveReturned(undefined);
  });
});

describe('Test AsyncArray forEachSeries', () => {
  test('forEachSeries calls the callback for each item.', async () => {
    const fn = jest.fn();
    await items.series(fn);
    expect(fn).toHaveBeenCalledTimes(items.length);
    expect(fn).toHaveReturned(undefined);
  });

  test('forEachSeries calls the callback in order.', async () => {
    const fn = jest.fn();
    await items.series(fn);
    for (let i = 0; i < items.length; i++) {
      expect(fn).toHaveBeenNthCalledWith(i + 1, items[i], i, items);
    }
  });
});

describe('Test AsyncArray map.', () => {
  test('map calls the callback for each item and return in order.', async () => {
    const fn = jest.fn((v) => { return { result: v }; });
    const result = await items.map(fn);
    expect(fn).toHaveBeenCalledTimes(items.length);
    [].forEach.call(items, (i) => {
      expect(fn).toHaveBeenCalledWith(i, expect.any(Number), items);
    });
    const expResult = [].map.call(items, (v) => { return { result: v }; });
    expect(result).toEqual(expResult);
  });
});

describe('Test AsyncArray find.', () => {
  test('find finds expected object.', async () => {
    const expected = items[Math.floor(Math.random() * items.length)];
    const result = await items.find((item) => item === expected);
    expect(result).toEqual(expected);
  });
});

describe('Test AsyncArray findIndex', () => {
  test('find finds expected index.', async () => {
    const index = Math.floor(Math.random() * items.length);
    const expected = items[index];
    const result = await items.findIndex((item) => item === expected);
    expect(result).toEqual(index);
  });
});

describe('Test AsyncArray some.', () => {
  test('some returns true if exists.', async () => {
    const index = Math.floor(Math.random() * items.length);
    const expected = items[index];
    const result = await items.some((item) => item === expected);
    expect(result).toEqual(true);
  });

  test('some returns false if not exists.', async () => {
    const result = await items.some((item) => item === 'abc123abc123');
    expect(result).toEqual(false);
  });
});

describe('Test AsyncArray every.', () => {
  test('Returns true if all is true.', async () => {
    const cb = () => true;
    const result = await items.every(cb);

    expect(result).toBe(true);
  });
  test('Returns false if any is false.', async () => {
    const cb = (i) => i !== null;
    const result = await items.every(cb);

    expect(result).toBe(false);
  });
});
