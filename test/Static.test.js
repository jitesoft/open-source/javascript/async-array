import { every, find, findIndex, forEach, forEachSeries, map, some } from '../src/Static';

const items = ['a', 'b', 3, 7, 'hej', { a: 'b' }, null, 41, 'hi!', 'cat', { spec: 'dog', ears: 1 }];

describe('Tests static forEach.', () => {
  test('Foreach calls the callback for each item.', async () => {
    const fn = jest.fn();
    await forEach(items, fn);
    expect(fn).toHaveBeenCalledTimes(items.length);

    items.forEach((i) => {
      expect(fn).toHaveBeenCalledWith(i, expect.any(Number), items);
    });
    expect(fn).toHaveReturned(undefined);
  });
});

describe('Test static forEachSeries', () => {
  test('forEachSeries calls the callback for each item.', async () => {
    const fn = jest.fn();
    await forEachSeries(items, fn);
    expect(fn).toHaveBeenCalledTimes(items.length);
    expect(fn).toHaveReturned(undefined);
  });

  test('forEachSeries calls the callback in order.', async () => {
    const fn = jest.fn();
    await forEachSeries(items, fn);
    for (let i = 0; i < items.length; i++) {
      expect(fn).toHaveBeenNthCalledWith(i + 1, items[i], i, items);
    }
  });
});

describe('Test static map.', () => {
  test('map calls the callback for each item and return in order.', async () => {
    const fn = jest.fn((v) => { return { result: v }; });
    const result = await map(items, fn);
    expect(fn).toHaveBeenCalledTimes(items.length);
    items.forEach((i) => {
      expect(fn).toHaveBeenCalledWith(i, expect.any(Number), items);
    });
    expect(fn).toHaveReturned(undefined);
    const expResult = items.map((v) => { return { result: v }; });
    expect(result).toEqual(expResult);
  });
});

describe('Test static find.', () => {
  test('find finds expected object.', async () => {
    const expected = items[Math.floor(Math.random() * items.length)];
    const result = await find(items, (item) => item === expected);
    expect(result).toEqual(expected);
  });
});

describe('Test static findIndex', () => {
  test('find finds expected index.', async () => {
    const index = Math.floor(Math.random() * items.length);
    const expected = items[index];
    const result = await findIndex(items, (item) => item === expected);
    expect(result).toEqual(index);
  });
});

describe('Test static some.', () => {
  test('some returns true if exists.', async () => {
    const index = Math.floor(Math.random() * items.length);
    const expected = items[index];
    const result = await some(items, (item) => item === expected);
    expect(result).toEqual(true);
  });

  test('some returns false if not exists.', async () => {
    const result = await some(items, (item) => item === 'abc123abc123');
    expect(result).toEqual(false);
  });
});

describe('Test static every.', () => {
  test('Returns true if all is true.', async () => {
    const cb = () => true;
    const result = await every(items, cb);

    expect(result).toBe(true);
  });
  test('Returns false if any is false.', async () => {
    const cb = (i) => i !== null;
    const result = await every(items, cb);

    expect(result).toBe(false);
  });
});
